﻿using System;
using System.Windows.Forms;
using System.IO;

namespace RemoveLogFiles
{
    public partial class FrmMain : Form
   {
      public FrmMain()
      {
         InitializeComponent();
      }

      private void FrmMain_Load(object sender, EventArgs e)
      {
            try
            {
                //C:\inetpub\logs\LogFiles\W3SVC1  on naboo and balmorra
                DateTime today = DateTime.Now;
                string dyrLog = @"\\goober\logs$";
                DirectoryInfo drInfo = new DirectoryInfo(dyrLog);
                foreach (var dyrectory in drInfo.GetDirectories())
                {
                    string[] fyles = Directory.GetFiles(String.Format(@"{0}\{1}", dyrLog, dyrectory), "*.log");
                    foreach (var fyle in fyles)
                    {
                        FileInfo fylInfo = new FileInfo(fyle);    //String.Format(@"{0}\{1}\{2}", dyrLog, dyrectory, fyle));
                        if ((today - fylInfo.CreationTime).Days > 30)                          //45 days for naboo and balmorra
                        {
                            File.Delete(fyle);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         Application.Exit();
      }
   }
}
